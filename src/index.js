import { React } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Web3ReactProvider } from "@web3-react/core";
import Web3 from "web3";
import SnackbarProvider from "react-simple-snackbar";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "remixicon/fonts/remixicon.css";
import "./index.css";

import Home from "./pages/Home";

const root = createRoot(document.getElementById("root"));

const getLibrary = (provider) => {
	return new Web3(provider);
};

export default function App() {
	return (
		<BrowserRouter>
			<Routes>
				<Route index element={<Home />} />
			</Routes>
		</BrowserRouter>
	);
}

root.render(
	<Web3ReactProvider getLibrary={getLibrary}>
		<SnackbarProvider>
			<App />
		</SnackbarProvider>
	</Web3ReactProvider>
);
